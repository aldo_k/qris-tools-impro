package main

import (
	"flag"
	"fmt"

	// "log"
	"strconv"
)

func main() {
	// inisiasi Kertas
	perintah := flag.String("act", "", "Perintah List : PDF | CLEAN | RESIZE")
	margin := flag.String("margin", "0.5", "Margin Kertas (CM)")
	lebar := flag.String("lebar", "32.5", "Lebar Kertas Cetak PDF (CM)")
	tinggi := flag.String("tinggi", "48", "Tinggi Kertas Cetak PDF (CM)")
	uk := flag.String("uk", "A6", "Ukuran Resize (A5/A6/A7)")
	flag.Parse()

	if *perintah == "" {
		fmt.Println("[INFO] Tidak Ada Perintah Yg Diterima [ Ex : ./dev.exe --act CLEAN ]")
		flag.PrintDefaults()
		return
	}

	marginKertas, _ := strconv.ParseFloat(*margin, 64)
	marginKertas = cmToPoint(marginKertas)

	lebarKertas, _ := strconv.ParseFloat(*lebar, 64)
	lebarKertas = cmToPoint(lebarKertas)

	tinggiKertas, _ := strconv.ParseFloat(*tinggi, 64)
	tinggiKertas = cmToPoint(tinggiKertas)

	if *perintah == "CLEAN" {
		cleanDir()
		return
	}

	if *perintah == "RESIZE" {
		resizeFile(*uk)
		return
	}

	if *perintah == "PDF" {
		widthImg := 209.76
		heightImg := 297.64
		folderPath := "A7"
		generatePDF(lebarKertas, tinggiKertas, marginKertas, widthImg, heightImg, folderPath)

		widthImg = 297.64
		heightImg = 419.53
		folderPath = "A6"
		generatePDF(lebarKertas, tinggiKertas, marginKertas, widthImg, heightImg, folderPath)

		widthImg = 419.53
		heightImg = 595.28
		folderPath = "A5"
		generatePDF(lebarKertas, tinggiKertas, marginKertas, widthImg, heightImg, folderPath)
		return
	}

	fmt.Println("[INFO] Tidak Ada Perintah Yg Diterima [ Ex : ./dev.exe --act CLEAN ]")
	flag.PrintDefaults()
}
