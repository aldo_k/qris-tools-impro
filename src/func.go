package main

import (
	"math"
)

func cmToPoint(cm float64) float64 {
	return cm * 28.3465
}

func cmToPixel(cm float64) int {
	return int(math.Round(cm * 37.795275591))
}
