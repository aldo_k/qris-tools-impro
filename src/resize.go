package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/disintegration/imaging"
)

func resizeFile(folderName string) {
	fmt.Println("[INFO] Resize File Start")

	// Default Uk A4
	widthImg := 21.0
	heightImg := 29.7
	if folderName == "A7" {
		widthImg = 7.4
		heightImg = 10.5
	}

	if folderName == "A6" {
		widthImg = 10.5
		heightImg = 14.8
	}

	if folderName == "A5" {
		widthImg = 14.8
		heightImg = 21.0
	}

	filepath.Walk("FileCetak/"+folderName, func(path string, fa7 os.FileInfo, err error) error {
		if !fa7.IsDir() {
			fileExtension := filepath.Ext(path)
			if fileExtension == ".png" {
				fmt.Println(path)
				// Open a test image.
				src, err := imaging.Open(path)
				if err != nil {
					log.Fatalf("failed to open image: %v", err)
				}

				src = imaging.Resize(src, cmToPixel(widthImg), cmToPixel(heightImg), imaging.Lanczos)
				// Save the resulting image as JPEG.
				err = imaging.Save(src, "ResizeImg/"+folderName+"/"+filepath.Base(path))
				if err != nil {
					log.Fatalf("failed to save image: %v", err)
				}
			}
		}
		return nil
	})

	fmt.Println("[INFO] Resize File Finished")
}
