package main

import (
	"fmt"
	// "log"
	"os"
	"path/filepath"
)

func cleanDir() {
	fmt.Println("[INFO] Sedang Membersihkan Direktori [ 0 / 4 ]")

	removeFile("FileCari/")
	fmt.Println("[INFO] Folder FileCari Selesai Dibersihkan [ 1 / 4 ]")

	removeFile("FileCetak/")
	fmt.Println("[INFO] Folder FileCetak Selesai Dibersihkan [ 2 / 4 ]")

	removeFile("HasilPDF/")
	fmt.Println("[INFO] Folder HasilPDF Selesai Dibersihkan [ 3 / 4 ]")

	removeFile("ResizeImg/")
	fmt.Println("[INFO] Folder ResizeImg Selesai Dibersihkan [  4 / 4 ]")

	fmt.Println("[INFO] Selesai Membersihkan Direktori [ Done ]")
}

func removeFile(folderName string) bool {
	if folderName == "" {
		return false
	}

	filepath.Walk(folderName, func(path string, fa7 os.FileInfo, err error) error {
		if !fa7.IsDir() {
			fileExtension := filepath.Ext(path)
			if fileExtension == ".zip" || fileExtension == ".png" || fileExtension == ".pdf" {
				err := os.Remove(path)
				if err != nil {
					fmt.Println(err)
					return nil
				}
			}
		}
		return nil
	})

	return true
}
