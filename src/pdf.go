package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/signintech/gopdf"
)

type CsvListQris struct {
	PaperSize  string
	FolderName string
	FileName   string
}

func generatePDF(lebarKertas float64, tinggiKertas float64, margin float64, moveleft float64, movedown float64, folderPath string) {

	searchDir := "FileCetak/" + folderPath
	fileList := []string{}
	ListFolder := map[string][]string{}

	// Hitung Jumlah Gambar Yg Dapat Dibuat
	// hitungMuatan = nil

	// Posisi Gambar Awal Inisiasi
	totalSamping := margin
	totalBawah := margin

	sizeImg := &gopdf.Rect{W: moveleft, H: movedown}
	loop := 0

	oldDir := ""
	// For Unzip File
	filepath.Walk(searchDir, func(path string, f os.FileInfo, err error) error {
		if !f.IsDir() {
			fileExtension := filepath.Ext(path)
			fileDir := filepath.Dir(path)

			if oldDir == "" {
				oldDir = fileDir
			}

			if fileExtension == ".png" {
				fileList = append(fileList, path)
				ListFolder[fileDir] = append(ListFolder[fileDir], path)
				if fileDir != oldDir {
					fileList = []string{}
				}
			}

			if fileExtension == ".zip" {
				uz := New()

				_, err := uz.Extract(path, fileDir)
				if err != nil {
					fmt.Println(err)
				}
			}

		}
		return nil
	})

	csvFileName := "HasilPDF/List-File-Uk.-" + folderPath + ".csv"

	file, err := os.Create(csvFileName)

	if err != nil {
		log.Fatalln("failed to open file", err)
	}

	defer file.Close()

	w := csv.NewWriter(file)

	defer w.Flush()

	var csvListQris []CsvListQris

	csvListQris = append(csvListQris, CsvListQris{
		PaperSize:  "Ukuran",
		FolderName: "Folder",
		FileName:   "Nama File",
	})

	for key, value := range ListFolder {
		printpdf := strings.Replace(key, "\\", " ", -1)
		printpdf = strings.Replace(printpdf, "FileCetak ", "", -1)
		fmt.Println("=====================================================================")
		fmt.Println("[INFO] Qris Uk. "+folderPath+" :", printpdf+".pdf [Mulai]")

		pdf := gopdf.GoPdf{}
		pdf.Start(gopdf.Config{PageSize: gopdf.Rect{W: lebarKertas, H: tinggiKertas}}) //595.28, 841.89 = A4

		jumlahFile := len(value)
		err := pdf.AddTTFFont("loma", "apps/ttf/Loma.ttf")
		if err != nil {
			log.Print(err.Error())
			return
		}

		err = pdf.SetFont("loma", "", 12)
		if err != nil {
			log.Print(err.Error())
			return
		}

		totalSamping = margin
		totalBawah = margin
		marginHelper := margin + (14.1732 / 2)
		totalPages := 0

		for _, path := range value {
			fileExtension := filepath.Ext(path)

			csvListQris = append(csvListQris, CsvListQris{
				PaperSize:  folderPath,
				FolderName: filepath.Dir(path),
				FileName:   filepath.Base(path),
			})

			if fileExtension == ".png" {
				if totalSamping == margin && totalBawah == margin {
					// New Page : pdf.AddPage()
					fmt.Println("=====================================================================")
					pdf.AddPage()

					totalPages++

					totalBawah += 14.1732
					info := "Uk. " + printpdf + " ( " + strconv.Itoa(jumlahFile) + " QRIS ) Hal " + strconv.Itoa(totalPages)
					pdf.SetX(marginHelper)
					pdf.SetY(marginHelper)
					pdf.Text(info)

				}

				pdf.SetLineWidth(0.1)
				pdf.SetLineType("dashed")
				pdf.Line(totalSamping, marginHelper, totalSamping, (tinggiKertas - marginHelper))

				pdf.SetLineWidth(0.1)
				pdf.SetLineType("dashed")
				pdf.Line(totalSamping+moveleft, marginHelper, totalSamping+moveleft, (tinggiKertas - marginHelper))

				pdf.SetLineWidth(0.1)
				pdf.SetLineType("dashed")
				pdf.Line(marginHelper, totalBawah, (lebarKertas - marginHelper), totalBawah)

				pdf.SetLineWidth(0.1)
				pdf.SetLineType("dashed")
				pdf.Line(marginHelper, totalBawah+movedown, (lebarKertas - marginHelper), totalBawah+movedown)

				fmt.Println("[INFO] Menambahkan File :", filepath.Base(path))
				//print image
				pdf.Image(path, totalSamping, totalBawah, sizeImg)

				if (totalSamping + (moveleft * 2)) < lebarKertas-margin {
					totalSamping += moveleft

				} else if (totalBawah + (movedown * 2)) < tinggiKertas-margin {
					totalSamping = margin
					totalBawah += movedown
				} else {
					totalSamping = margin
					totalBawah = margin
				}
				loop++
			}

		}

		pdf.WritePdf("HasilPDF/" + printpdf + ".pdf")
		fmt.Println("=====================================================================")
		fmt.Println("[INFO] Qris Uk. "+folderPath+" : [", printpdf+".pdf ] [Selesai] [Total", loop, "Qris]")
		fmt.Println("=====================================================================")
		loop = 0
	}

	var data [][]string
	for _, record := range csvListQris {
		row := []string{record.PaperSize, record.FolderName, record.FileName}
		data = append(data, row)
	}
	w.WriteAll(data)
}
